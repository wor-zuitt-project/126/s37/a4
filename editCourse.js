let params = new URLSearchParams(window.location.search)

let courseId = params.get("courseId")

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDescription");
let coursePrice = document.querySelector("#coursePrice");
let token = localStorage.getItem("token"); 

fetch(`http://localhost:4000/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	// console.log(data)
	courseName.value = data.name;
	courseDesc.value = data.description;
	coursePrice.value = data.price;

	document.querySelector("#editCourse").addEventListener("submit", (e)=> {
		e.preventDefault()

		//variables representing the values of our inputs
		let name = courseName.value
		let desc = courseDesc.value
		let price = coursePrice
		let token = localStorage.getItem("token")

		fetch(`http://localhost:4000/courses/${courseId}`,{
					method: 'PUT',
					headers: {
							Authorization: `Bearer ${token}`,
							'Content-Type': 'application/json'
						},
					body: JSON.stringify({
						name: name,
						description: desc,
						price: price
					})	
		})
	
	.then(res => res.json())
	.then (data => {
			if(data === true){
						console.log(data)
						alert("Successfully update the Course.")
						window.location.replace("./courses.html")
					}else{
						alert("Update Course Fail. Please Try Again")
					}
		})


})

})


 